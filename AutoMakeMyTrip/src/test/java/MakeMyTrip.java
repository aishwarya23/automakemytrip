
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import org.openqa.selenium.OutputType;

import org.openqa.selenium.TakesScreenshot;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.support.ui.Select;

import com.google.common.io.Files;

import io.github.bonigarcia.wdm.WebDriverManager;

public class MakeMyTrip {

	static WebDriver wd;

	public static void main(String[] args) throws IOException, InterruptedException {

		// Step1:Launch the Website

		wd=WebDriverManager.chromedriver().create();
		wd.navigate().to("https://www.makemytrip.com/");
		wd.manage().window().maximize();
		wd.manage().deleteAllCookies();
		wd.findElement(By.className("langCardClose")).click();

		// Step2:Select any one option
		
		wd.findElement(By.xpath("//label[@for='fromCity']")).click();
		wd.findElement(By.xpath("//input[@placeholder='From']")).sendKeys("Bengaluru");
		Thread.sleep(2000);
		
		wd.findElement(By.xpath("//li[@id='react-autowhatever-1-section-0-item-0']")).click();
		Thread.sleep(5000);
		wd.findElement(By.xpath("//label[@for='toCity']")).click();
		wd.findElement(By.xpath("//input[@placeholder='To']")).sendKeys("DPS");
		Thread.sleep(5000);
		wd.findElement(By.xpath("//li[@id='react-autowhatever-1-section-0-item-0']")).click();
		
		// Step 4: Select the Departure date
		wd.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		wd.findElement(By.xpath("//*[contains(@aria-label,'Sep 12')]")).sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		wd.findElement(By.xpath("(//*[@class=\"lbl_input latoBold appendBottom10\"])[2]")).click();
		wd.findElement(By.xpath("//*[contains(@aria-label,'Sep 17')]")).sendKeys(Keys.ENTER, Keys.PAGE_UP);
		wd.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		File src = ((TakesScreenshot) wd).getScreenshotAs(OutputType.FILE);
		File f = new File("C:\\Users\\Aishwarya\\review.png");
		Files.copy(src, f);

		// Step 5: Search for the cheapest price and click on Book now button
		wd.findElement(By.xpath("//a[@class='primaryBtn font24 latoBold widgetSearchBtn ' and text()='Search']")).click();
		// wd.findElement(By.xpath("//label[@for='split_1_120_0']")).click();
		wd.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		wd.findElement(By.xpath("(//*[text()=\"View Prices\"])[1]")).click();
		wd.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		wd.findElement(By.xpath("//*[text()='Continue']")).click();
		// Step 6: Review the selection in the review page
		ArrayList<String> ls1 = new ArrayList<String>(wd.getWindowHandles());
		wd.switchTo().window(ls1.get(ls1.size() - 1));
		wd.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		File src1 = ((TakesScreenshot) wd).getScreenshotAs(OutputType.FILE);
		File f1 = new File("C:\\Users\\Aishwarya\\review1.png");
		Files.copy(src1, f1);
		wd.quit();

	}

}
